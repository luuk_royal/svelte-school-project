import sirv from 'sirv';
import compression from 'compression';
import * as sapper from '@sapper/server';
import express from 'express';
import api from "./api/routes/api";
import login from "./api/routes/login";

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';
const app = express();

app.use(express.json());
app.use('/api', api);
app.put('/credentials', login);

app.use(
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware()
	);

app.listen(PORT, err => {
		if (err) console.log('error', err);
	});

