import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

//voor login en tokens

/**
 * Encrypts the password with bcrypt
 *
 * @param plainPassword the password in text
 * @returns {*} the encrypted password
 */
export function makeHashPassword(plainPassword) {
    return bcrypt.hashSync(plainPassword, bcrypt.genSaltSync(10));
}

/**
 * Compares the passwords by looking at the sync
 *
 * @param plainPassword the password in text
 * @param hash the password hashed
 * @returns {*} true if the are the same
 */
export function checkPassword(plainPassword, hash){
    return bcrypt.compareSync(plainPassword, hash);
}

const mySecret = 'My Secret on the Server side', bearerPrefix = 'Bearer ';

/**
 * Creates the token you need to interact with the api
 *
 * @param payload contains the name and roles of the user
 * @returns {undefined|*}
 */
export function createToken(payload){
    return jwt.sign(payload, mySecret);
}

/**
 * Verifies the token by checking the secret and the bearerPrefix
 *
 * @param authorization the string containing the token
 * @returns {boolean|*} returns true if the token is valid
 */
export function verifyToken(authorization){
    return authorization.startsWith(bearerPrefix) && jwt.verify(authorization.substr(bearerPrefix.length), mySecret);
}

/**
 * Gets the payload out of the token that is inside of the header
 *
 * @param req the request with corresponding data
 * @returns {null|{payload: *, signature: *, header: *}} the decoded token
 */
export function getPayloadFromHeader(req){
    const token = req.header("Authorization").substring(7);
    console.log(token);
    return  jwt.decode(token);
}