import status from 'http-status-codes';
import {checkPassword, createToken} from '../util.js';
import {getUser} from './users.js';

const {FORBIDDEN,} = status;

/**
 *  This takes care of the login
 *  It tries to get the user from the body, if theres no user or password it will give an error
 *  If theres a user and correct password, the token is created
 *
 * @param req gets the request
 * @param res gets the response
 */
export default function login(req, res) {
    const user = getUser(req.body?.name);

    if (!user || !checkPassword(req.body.password, user.passWordHash)) {

        res.status(FORBIDDEN).end('invalid users/password combination');

    } else {
        const payload = {
            user: user.name,
            roles: user.roles
        };
        res.json({token: createToken(payload)});
    }
}