import express from 'express';
import status from "http-status-codes";
import {getPayloadFromHeader} from "../util.js";
import {getUsers} from "./users.js";

const router = new express.Router();

const {NOT_FOUND, UNPROCESSABLE_ENTITY, CREATED, UNAUTHORIZED} = status;


function getDate() {
    let d = new Date();
    let hour = d.getHours();
    let min = d.getMinutes();

    if (min < 10) {
        let minutes = "0" + min

        return hour + ":" + minutes
    } else {
        return hour + ":" + min;
    }
}

let itemId = 1;

let currentTime = getDate();

const bid = {bidder: 'adminUser', itemName: 'gele vaas', itemId: 0, bid: 15, when: currentTime, bestBid: true}

const auctionObjects = [
    //you can search (for parts of) the name, the description and the category
    {
        itemName: 'gele vaas',
        itemId: '0',
        endTimeAuction: "15:00",
        description: 'een gele vaas met blauwe bloemen',
        category: "vaas",
        highestBid: undefined,
        bids: [bid]
    },
    {
        itemName: 'Uitje Twente',
        itemId: '1',
        endTimeAuction: "16:00",
        description: 'Een rit op een party wagen achter een trekker die eindigt bij een restaurant waar je gaat eten.',
        category: "uitje",
        highestBid: undefined,
        bids: [{
            bidder: 'userUser',
            itemName: 'Uitje Twente',
            itemId: '1',
            bid: 22,
            when: currentTime,
            bestBid: true
        }, {
            bidder: 'adminUser',
            itemName: 'Uitje Twente',
            itemId: '1',
            bid: 10,
            when: currentTime,
            bestBid: false
        }]
    }
]

// const bids = [bid]

/**
 * Gets all of the items that are currently listed
 * Has a filer so you can search for objects
 */
router.get('/', (req, res) => {
    const {search} = req.query;

    console.log(search);

    //nakijken reguliere expressies
    res.json(auctionObjects.filter(object =>
        (!search || new RegExp(search.toLowerCase()).test(object.itemName.toLowerCase()) ||
            new RegExp(search.toLowerCase()).test(object.description.toLowerCase()) ||
            new RegExp(search.toLowerCase()).test(object.category.toLowerCase()))
    ));
});


/**
 * The code checks if an item exists and if it does it gets the item
 */
router.get('/:itemId', (req, res) => {
    const id = req.params.itemId;
    const auctionObject = auctionObjects.find(candidate => candidate.itemId === id);

    if (auctionObject) {
        res.json(auctionObject);
    } else {
        res.status(NOT_FOUND).end(`item ${id} doesn't exist`);
    }
});

/**
 * If a post is made to an auction object i expect a bid as that is the only thing that can be posted to an item
 * It simply checks if the bid already exists and if it does not it adds the bid
 * It also checks if the bid is the highest bid yet
 */
router.post('/:itemId/bids', (req, res) => {
    let bid = req.body;
    let bidder = bid.bidder;
    let amount = bid.bid;
    let itemName = bid.itemName;
    let itemId = bid.itemId;
    let auctionObject = auctionObjects.find(candidate => candidate.itemId === bid.itemId)

    console.log(bid);

    if (auctionObject.bids.find(bid => bid.bidder === bidder && bid.bid === amount && bid.itemName === itemName)) {
        res.status(UNPROCESSABLE_ENTITY).end(`bid by ${bid.bidder} already exists`);
    } else {
        let currentTime = getDate();

        let finalBid = {
            bidder: bidder,
            itemName: itemName,
            itemId: itemId,
            bid: amount,
            when: currentTime,
            bestBid: false
        }

        console.log(auctionObject.highestBid);

        const highestBid = auctionObject.highestBid;

        if (highestBid === undefined) {
            auctionObject.highestBid = finalBid;

            finalBid.bestBid = true;
        } else if (finalBid.bid > highestBid.bid) {
            auctionObject.bids.find(bid => bid.bidder === highestBid.bidder && bid.bid === highestBid.bid).bestBid = false;
            finalBid.bestBid = true;
            auctionObject.highestBid = finalBid;

            console.log(finalBid.bestBid);
        }

        let users = getUsers();
        users.find(candidate => candidate.name === bidder).bids.push(finalBid);

        auctionObjects.find(candidate => candidate.itemId === bid.itemId).bids.push(finalBid);
        res.json(finalBid); //The status and the accepted code has been removed because the sendJson method saw it as an error
    }
})

/**
 * This code checks if the auctionObject exists and if the person trying to remove the object is an admin, if those are both true the item is deleted
 */
router.delete('/:itemId', (req, res) => {
    const removeAuctionObject = req.body;
    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');

    console.log(removeAuctionObject);
    const index = auctionObjects.findIndex(auctionObject => auctionObject.itemId === removeAuctionObject.itemId);
    if (auctionObjects.find(candidate => candidate.itemId === removeAuctionObject.itemId) && admin) {

        auctionObjects.splice(index, 1);
        res.json(auctionObjects);
    } else if (index < 0) {
        res.status(UNPROCESSABLE_ENTITY).end(`${removeAuctionObject} doesn't exist`);
    } else {
        res.status(UNAUTHORIZED).end('you do not have permission to acces this');
    }
})

/**
 * This code checks if the bid exists and if the person trying to remove the object is an admin, if those are both true the item is deleted
 */
router.delete('/:itemId/bids', (req, res) => {
    const removeBid = req.body;
    const payload = getPayloadFromHeader(req);
    const username = payload.user;
    const admin = payload.roles.includes('admin');

    if (username === removeBid.bidder || admin) {

        let users = getUsers();
        let user = users.find(candidate => candidate.name === removeBid.bidder);
        let userIndex = user.bids.findIndex(bid => bid.itemId === removeBid.itemId &&
            bid.bidder === removeBid.bidder && bid.bid === removeBid.bid);

        let auctionObject = auctionObjects.find(candidate => candidate.itemId === removeBid.itemId);

        console.log(auctionObject);
        //Checks if the auctionObject has been removed.
        if (auctionObject === undefined) {
        } else {
            let auctionIndex = auctionObject.bids.findIndex(bid =>
                bid.itemId === removeBid.itemId && bid.bidder === removeBid.bidder &&
                bid.bid === removeBid.bid);

            auctionObject.bids.splice(auctionIndex, 1);

            //sorts the array in case the highest bid is deleted and sets the highest bid at first place as the highest bid.
            auctionObject.bids.sort((a, b) => {
                if (a.bid > b.bid) {
                    return -1;
                } else {
                    return 1;
                }
            });

            auctionObject.bids.forEach(function (bid) {
                bid.bestBid = false;
            });

            auctionObject.bids[0] = true;

            auctionObject.highestBid = auctionObject.bids[0];
        }

        user.bids.splice(userIndex, 1);


        res.json(removeBid);
    } else if (removeBid.bidder !== username) {
        res.status(UNAUTHORIZED).end(`${username} can not remove others bids`);
    } else {
        res.status(NOT_FOUND).end(`The bid does not exist.`);
    }
});

function getItemId() {
    return itemId + 1;
}

/**
 * This code is responsible for adding items to the list
 * If te object already exists it will give an UNPROCESSABLE_ENTITY status code back
 * If the user isn't an admin it will refuse and give an UNAUTHORIZED status code back
 */
router.post('/', (req, res) => {
    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');

    const object = req.body;
    console.log(object);
    if (auctionObjects.find(candidate => candidate.itemName === object.name) && admin) {
        res.status(UNPROCESSABLE_ENTITY).end(`item with name ${object.name} already exists`);
    } else if (!admin) {
        res.status(UNAUTHORIZED).end('you do not have permission to use this');
    } else {

        let finishedObject = ({
            "itemName": object.itemName.replace(' ', '_'),
            "itemId": getItemId().toString(),
            "endTimeAuction": object.endTimeAuction,
            "description": object.description,
            "category": object.category,
            "highestBid": undefined,
            "bids": []
        });

        console.log(finishedObject);

        auctionObjects.push(finishedObject);
        res.status(CREATED).json(object);
    }
})

/**
 * This code updates an item once it receives the request with a valid item, url and if the user is an admin
 * If the user isn't an admin it will refuse and give an UNAUTHORIZED status code back
 * If te object doesn't exist it will give a NOT_FOUND status code back
 */
router.put('/:itemId', (req, res) => {
    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');


    const object = req.body;
    const id = req.params.itemId;
    let auctionObject = auctionObjects.find(candidate => candidate.itemId === id);

    if (auctionObject && admin) {
        Object.assign(auctionObject, object);
        res.json(auctionObject);
    } else if (!admin) {
        res.status(UNAUTHORIZED).end('you do not have permission to use acces this');
    } else {
        res.status(NOT_FOUND).end(`item ${id} doesn't exist`);
    }
})

export default router;