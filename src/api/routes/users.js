import express from 'express';
import status from 'http-status-codes';
import {getPayloadFromHeader, makeHashPassword} from "../util.js";

const router = new express.Router();

const {NOT_FOUND, UNPROCESSABLE_ENTITY, CREATED, UNAUTHORIZED} = status;

const users = [
    //sync gebruikt
    {
        name: 'Dick',
        email: 'Dick123@saxion.nl',
        //c0rrect
        passWordHash: '$2b$10$fh.jZZybhGuONnEd5tOlD.wHXFEUO/Eal8dXQFrbWM2YN2DVxLSQa',
        roles: ['user', 'admin'],
        auctionsWon: [],
        bids: []
    },
    {
        name: 'Richard', email: 'Richard123@saxion.nl', passWordHash: '', roles: ['user'],
        auctionsWon: [],
        bids: []
    },
    {
        name: 'Luuk', email: '475085@student.saxion.nl', passWordHash: '', roles: ['user', 'admin'],
        auctionsWon: [],
        bids: []
    },
];

/**
 * Controls if the user trying to acces the user list has admin privileges
 */
router.get('/', (req, res) => {
    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');

    if (admin) {
        res.json(users);
    } else {
        res.status(UNAUTHORIZED).end('You need admin privileges for this request!');
    }
});

/**
 * Gets all of the users
 *
 * @returns {[{auctionsWon: [], roles: [string], name: string, passWordHash: string, email: string}, {roles: [string], name: string, passWordHash: string, email: string}, {roles: [string, string], name: string, passWordHash: string, email: string}]}
 */
export function getUsers() {
    return users;
}

/**
 * Gets a certain user
 *
 * @param username
 * @returns {{auctionsWon: [], roles: [string, string], name: string, passWordHash: string, email: string} | {roles: [string], name: string, passWordHash: string, email: string} | {roles: [string, string], name: string, passWordHash: string, email: string}}
 */
export function getUser(username) {
    return users.find(candidate => candidate.name === username);
}

/**
 * Gets the auctions that the user has won from the user
 *
 * @param user the user
 * @returns {[]} the actions the user has won
 */
function getAuctionsWon(user) {
    return user.auctionsWon;
}

/**
 * Gets the auctions the user has won and sends them back in a json format
 */
router.get('/:name/auctionsWon', (req, res) => {
    const name = req.params.name;
    const user = users.find(candidate => candidate.name === name);

    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');

    if (user) {
        res.json(getAuctionsWon(user));
    } else if (admin) {
        res.json(getAuctionsWon(user));
    } else {
        res.status(NOT_FOUND).end(`gebruiker met naam ${name} bestaat niet`);
    }
})

/**
 * Adds the auction that the user won to their account
 */
router.post('/:name/auctionsWon', (req, res) => {
    const name = req.params.name;
    const auctionWon = req.body;
    const user = users.find(candidate => candidate.name === name);

    const payload = getPayloadFromHeader(req);
    const admin = payload.roles.includes('admin');

    if (user) {
        user.auctionsWon.push(auctionWon);
        res.status(CREATED).end(`${user.name} has won: \n ${auctionWon.itemName}`);
    } else if (admin) {
        user.auctionsWon.push(auctionWon);
        res.status(CREATED).end(`${user.name} has won: \n ${auctionWon.itemName}`);
    } else {
        res.status(NOT_FOUND).end(`gebruiker met naam ${name} bestaat niet`);
    }
})

/**
 * Gets the user with the corresponding name
 */
router.get('/:name', (req, res) => {
    const name = req.params.name;
    console.log(name);
    const user = users.find(candidate => candidate.name === name);
    console.log(user);
    const admin = req.token.roles.includes('admin');
    if (user || admin) {
        res.json(user);
    } else {
        res.status(NOT_FOUND).end(`gebruiker met naam ${name} bestaat niet`);
    }
})

/**
 * Code that gets the data it needs from the body and then puts the correct data + the hashed password into a new object so the password is stored in hash
 */
router.post('/', (req, res) => {
    const newUser = req.body;
    console.log(newUser);
    if (users.find(candidate => candidate.name === newUser.name)) {
        console.log(users.find(candidate => candidate.name === newUser.name));
        res.status(UNPROCESSABLE_ENTITY).end(`gebruiker met naam ${newUser.name} bestaat al`);
    } else {
        const newHashPassword = makeHashPassword(newUser.password);

        let finishedUser = ({
            "name": newUser.name,
            "email": newUser.email,
            "passWordHash": newHashPassword,
            "roles": ['user'],
            "auctionsWon": [],
            "bids": []
        });

        console.log(finishedUser + 'succes');

        users.push(finishedUser);
        res.json(finishedUser);
    }

})
export default router;