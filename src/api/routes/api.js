import express from 'express'
import users from './users.js'
import auctions from './auctions.js'
import {verifyToken} from "../util.js"
import status from 'http-status-codes'

//voor login
const {UNAUTHORIZED} = status
const router = new express.Router()

/**
 * Validates the token and adds the information that should be in the payload
 */
router.use(((req, res, next) => {

    console.log(req.originalUrl + " " + req.method);

    if (req.originalUrl === '/api/auctions/' && req.method === 'GET' ||
            req.originalUrl === '/api/users/' && req.method === 'POST' ||
                req.originalUrl.includes('/api/auctions/?search=') && req.method === 'GET') {
        return next();
    } else {
        try {
            const payload = verifyToken(req.header('Authorization'))
            if (payload) {
                req.token = payload;
                return next();
            }
        } catch (problem) {
        }
        res.status(UNAUTHORIZED).end('missing valid JSON web token');
    }
}))

/**
 * Sets up the correct routes
 */
router.use(express.json());
router.use('/users', users);
router.use('/auctions', auctions);

export default router;