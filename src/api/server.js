console.log('Express server opstarten...');
import login from "./routes/login.js";

import express from 'express';
import api from './routes/api.js';

const app = express();

/**
 * This tells the app to use json
 */
app.use(express.json());

/**
 * This sets the frontend up to be static
 */
app.use(express.static('./static'));

app.use('/api', api);

//voor login
/**
 * This makes sure the credentials are correct
 */
app.put('/credentials', login);

/**
 * This short piece of codes reveals if the server has started, if you don't see the text appear something went wrong
 *
 * @type {http.Server}
 */
const listener = app.listen(8081, () => {
    console.log(`Server started on port ${listener.address().port}.`);
})